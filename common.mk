.ONESHELL:
SHELL := bash
.SHELLFLAGS = -euo pipefail -c
FILES_POSTFIX := .openapi.yml
OPENAPI_MAKEFILE := /opt/makefile/Makefile
OPENAPI_DIR := .
OPENAPI_SRC_DIR := src
GIT_ROOT=$(shell realpath $$(git rev-parse --git-dir) | sed s'|/.git.*||')
GIT_SUBDIR=$(shell realpath --relative-to "$(GIT_ROOT)" "$(PWD)")
OPENAPI_CASING := camel
OPENAPI_PARAMS :=
SHARED_DIR := $(OPENAPI_DIR)/$(OPENAPI_SRC_DIR)/shared
FILES = $(wildcard $(OPENAPI_DIR)/*$(FILES_POSTFIX))
VERSIONS = $(FILES:$(OPENAPI_DIR)/%$(FILES_POSTFIX)=$(OPENAPI_DIR)/%.VERSION)
DOCKER_IMAGE_VERSION = 1
DOCKER_IMAGE := bananawhite98/openapi-generator:$(DOCKER_IMAGE_VERSION)
GROUP := stack
SPECTRAL_RULESET = $(SHARED_DIR)/ruleset.js
ARTIFACTORY_DEPLOY_URL :=
IBM_LINT_PARAMS :=
IMPORT_MAPPINGS_EXCLUDE_FILES :=
IMPORT_MAPPINGS_EXCLUDE_SCHEMAS :=

versions:
	@make docker-run MAKE_ARGS=versions

libs-npm: $(VERSION)
	@make docker-run MAKE_ARGS=libs-npm

libs-mvn-spring:
	@make docker-run MAKE_ARGS=".deps-mvn ARTIFACT=spring GROUP=$(GROUP)"

libs-mvn-feign:
	@make docker-run MAKE_ARGS=".deps-mvn ARTIFACT=feign GROUP=$(GROUP)"

clean:
	@make docker-run MAKE_ARGS="clean"

rules-make:
	@if [[ -z "$(MODULE)" ]]; then
		@printf "Error: Missing parameter MODULE\n"
		@printf "make $@ MODULE=<module name>\n"
		exit 1
	fi
	@echo "Rules to add to Makefile (with alphabetical order in mind)"
	@echo "$(MODULE)-java: $(MODULE)-spring $(MODULE)-feign"
	@echo
	@echo "$(MODULE)-spring:"
	@echo -e "\tmake docker-build VARIANT=spring MODULE=$(MODULE)"
	@echo -e "\tmake install-java VARIANT=spring MODULE=$(MODULE)"
	@echo
	@echo "$(MODULE)-feign:"
	@echo -e "\tmake docker-build VARIANT=feign MODULE=$(MODULE)"
	@echo -e "\tmake install-java VARIANT=feign MODULE=$(MODULE)"
	@echo
	@echo "$(MODULE)-lint:"
	@echo -e "\t@make docker-lint MODULE=$(MODULE)"
	@echo
	@echo "$(MODULE)-ts:"
	@echo -e "\tmake docker-build VARIANT=typescript MODULE=$(MODULE)"

rules-ci:
	@if [[ -z "$(MODULE)" ]]; then
		@printf "Error: Missing parameter MODULE\n"
		@printf "make $@ MODULE=<module name>\n"
		exit 1
	@fi
	@echo "Rules to add to .gitlab-ci.yml (with alphabetical order in mind)"
	@echo "$(MODULE)_api_feign:"
	@echo "  extends: .api_java"
	@echo "  variables:"
	@echo "    STACK_CI_MODULE: $(MODULE)"
	@echo
	@echo "$(MODULE)_api_spring:"
	@echo "  extends: .api_java"
	@echo "  variables:"
	@echo "    STACK_CI_MODULE: $(MODULE)"
	@echo
	@echo "$(MODULE)_api_ts:"
	@echo "  extends: .api_ts"
	@echo "  variables:"
	@echo "    STACK_CI_MODULE: $(MODULE)"
	@echo
	@echo "$(MODULE)_lint:"
	@echo "  extends: .lint_openapi"
	@echo "  variables:"
	@echo "    STACK_CI_MODULE: $(MODULE)"

config-init: config/$(MODULE)-feign.yml config/$(MODULE)-spring.yml config/$(MODULE)-typescript.yml
	@set -e
	@if [[ -z "$(MODULE)" ]]; then
		@printf "Error: Missing parameter MODULE\n"
		@printf "make $@ MODULE=<module name>\n"
		exit 1
	fi
	@echo "Configuration copied. FIles should be edited."
	@ls config/$(MODULE)*.yml
	@echo
	@make --no-print-directory rules-name MODULE=$(MODULE)
	@echo
	@make --no-print-directory rules-ci MODULE=$(MODULE)

shell:
	docker run \
		-e "CI_PROJECT_DIR=/src" \
		-u "$$(id -u):$$(id -g)" \
		-v $(GIT_ROOT):/src \
		-e HOME=$$HOME \
		-v ~/.m2:$$HOME/.m2 \
		-v ~/.cache:/$$HOME/.cache \
		--dns=10.13.10.11 --dns=1.1.1.1 \
		--rm -it \
		--workdir "/src/$(GIT_SUBDIR)" \
		$(DOCKER_IMAGE):$(DOCKER_IMAGE_VERSION) bash

docker-pull:
	docker pull $(DOCKER_IMAGE)

diagnose:
	@echo "Current commit:"
	git log -1
	@echo -e '\nApi version:'
	$(MAKE) --silent versions
	@echo -e '\nMake version:'
	make --version
	@echo -e '\nDocker version:'
	docker --version
	@echo -e '\nImage version (make docker-pull allows downloading latest image version):'
	@docker images | awk '/openapi-generator/ {if ($$2 == $(DOCKER_IMAGE_VERSION)){print}}'
	@echo -e "\nJava version inside an image"
	docker run --rm $(DOCKER_IMAGE) java --version
	@echo -e "\nOpenapi-generator version inside an image"
	docker run --rm $(DOCKER_IMAGE) openapi-generator --version
	@echo -e "\nibm-openapi-validator linter version inside an image"
	docker run --rm $(DOCKER_IMAGE) lint-openapi --version

lint-all:
	@for f_path in $(OPENAPI_SRC_DIR)/*$(FILES_POSTFIX); do
		filename=$$(basename "$${f_path}")
		stem="$${filename%%$(FILES_POSTFIX)}"
		$(MAKE) --silent "$${stem}-lint"
	done

java-all:
	@for f_path in $(OPENAPI_SRC_DIR)/*$(FILES_POSTFIX); do
		filename=$$(basename "$${f_path}")
		stem="$${filename%%$(FILES_POSTFIX)}"
		$(MAKE) --silent "$${stem}-java"
	done

ts-all:
	@for f_path in $(OPENAPI_SRC_DIR)/*$(FILES_POSTFIX); do
		filename=$$(basename "$${f_path}")
		stem="$${filename%%$(FILES_POSTFIX)}"
		printf "Building $${stem}\n"
		$(MAKE) --silent "$${stem}-ts"
	done

install-java:
	@if [[ -z "$(MODULE)" ]] || [[ -z "$(VARIANT)" ]]; then
		@printf "ERROR: Missing <MODULE> or <VARIANT> parameter\n"
		@printf "make $@ MODULE=<nazwa modułu> VARIANT=<nazwa wariantu>\n"
		exit 1
	fi
	MVN_OPTS="-Duser.home=$${HOME}"; \
	docker run \
		-e "CI_PROJECT_DIR=/src" \
		-e HOME=$$HOME \
		-u "$$(id -u):$$(id -g)" \
		-v $(GIT_ROOT):/src \
		-v ~/.m2:$$HOME/.m2 \
		-v ~/.cache:/$$HOME/.cache \
		--dns=10.13.10.11 --dns=1.1.1.1 \
		--workdir '/src/$(GIT_SUBDIR)' \
		--rm \
		$(DOCKER_IMAGE) \
		bash -c "make -f $(OPENAPI_MAKEFILE) \
			OPENAPI_DIR=/src/$(GIT_SUBDIR)/$(OPENAPI_DIR) \
			OPENAPI_SRC_DIR=$(OPENAPI_SRC_DIR) \
			FILES_POSTFIX=$(FILES_POSTFIX) \
			FILES=/src/$(GIT_SUBDIR)/$(OPENAPI_DIR)/$(OPENAPI_SRC_DIR)/$(MODULE)$(FILES_POSTFIX) \
			MVN_OPTS=\"$${MVN_OPTS}\" \
			EXTRA_DEPS='$(EXTRA_DEPS)' \
			install-$(VARIANT)"

docker-run:
	@docker run \
		-e "CI_PROJECT_DIR=/src" \
		-u "$$(id -u):$$(id -g)" \
		-v $(GIT_ROOT):/src \
		--dns=10.13.10.11 --dns=1.1.1.1 \
		--workdir '/src/$(GIT_SUBDIR)' \
		--rm \
		$(DOCKER_IMAGE) \
		bash -c "make -f $(OPENAPI_MAKEFILE) \
			ARTIFACT='$(ARTIFACT)' \
			GROUP='$(GROUP)' \
			OPENAPI_DIR=/src/$(GIT_SUBDIR)/$(OPENAPI_DIR) \
			OPENAPI_SRC_DIR=$(OPENAPI_SRC_DIR) \
			FILES_POSTFIX=$(FILES_POSTFIX) \
			$(MAKE_ARGS)"

docker-lint:
	@docker run \
		-e "CI_PROJECT_DIR=/src" \
		-u "$$(id -u):$$(id -g)" \
		-v $(GIT_ROOT):/src \
		--dns=10.13.10.11 --dns=1.1.1.1 \
		--workdir '/src/$(GIT_SUBDIR)' \
		--rm \
		$(DOCKER_IMAGE) \
		bash -c "make -f $(OPENAPI_MAKEFILE) \
			OPENAPI_DIR=/src/$(GIT_SUBDIR)/$(OPENAPI_DIR) \
			OPENAPI_SRC_DIR=$(OPENAPI_SRC_DIR) \
			FILES_POSTFIX=$(FILES_POSTFIX) \
			FILES=/src/$(OPENAPI_DIR)/src/${MODULE}$(FILES_POSTFIX) \
			SPECTRAL_RULESET=$(SPECTRAL_RULESET) \
			IBM_LINT_PARAMS='$(IBM_LINT_PARAMS)' \
			OPENAPI_CASING=$(OPENAPI_CASING) \
			lint"


docker-build:
	@task=java
	if [[ $(VARIANT) == "typescript" ]]; then
	    task=ts
	fi
	docker run \
		-e "CI_PROJECT_DIR=/src" \
		-e HOME=$$HOME \
		-u "$$(id -u):$$(id -g)" \
		-v "$(GIT_ROOT)":/src \
		-v ~/.m2:$$HOME/.m2 \
		-v ~/.cache:/$$HOME/.cache \
		--workdir '/src/$(GIT_SUBDIR)' \
		--dns=10.13.10.11 --dns=1.1.1.1 \
		--rm \
		$(DOCKER_IMAGE) \
		bash -c "make \
			OPENAPI_DIR=$(OPENAPI_DIR) \
			OPENAPI_SRC_DIR=$(OPENAPI_SRC_DIR) \
			OPENAPI_PARAMS='$(OPENAPI_PARAMS)' \
			FILES_POSTFIX=$(FILES_POSTFIX) \
			IMPORT_MAPPINGS_EXCLUDE_FILES='$(IMPORT_MAPPINGS_EXCLUDE_FILES)' \
			IMPORT_MAPPINGS_EXCLUDE_SCHEMAS='$(IMPORT_MAPPINGS_EXCLUDE_SCHEMAS)' \
			FILES=/src/$(GIT_SUBDIR)/$(OPENAPI_DIR)/$(OPENAPI_SRC_DIR)/${MODULE}$(FILES_POSTFIX) \
		        VARIANT=$(VARIANT) \
			MODULE=$(MODULE) \
			ARTIFACTORY_DEPLOY_URL=$(ARTIFACTORY_DEPLOY_URL) \
			EXTRA_DEPS='$(EXTRA_DEPS)' \
			JAVA_VERSION='$(JAVA_VERSION)' \
			ci-gen-$${task}"

ci-gen-java:
	SPEC="$(OPENAPI_DIR)/$(OPENAPI_SRC_DIR)/$(MODULE)$(FILES_POSTFIX)"
	MVN_OPTS="-s $${CI_PROJECT_DIR}/.mvn/settings.xml"; \
	if [[ ! -v CI ]]; then
		MVN_OPTS="-Duser.home=$${HOME}"; \
	fi
	make -f $(OPENAPI_MAKEFILE) \
		OPENAPI_DIR=$(OPENAPI_DIR) \
		OPENAPI_SRC_DIR=$(OPENAPI_SRC_DIR) \
		OPENAPI_PARAMS='$(OPENAPI_PARAMS)' \
		FILES_POSTFIX=$(FILES_POSTFIX) \
		FILES="$${SPEC}" \
		MVN_OPTS="$${MVN_OPTS}" \
		ARTIFACTORY_DEPLOY_URL=$(ARTIFACTORY_DEPLOY_URL) \
		JAVA_VERSION=$(JAVA_VERSION) \
		$(VARIANT)-api

ci-lint:
	SPEC="$(OPENAPI_DIR)/$(OPENAPI_SRC_DIR)/$(MODULE)$(FILES_POSTFIX)"
	make -f $(OPENAPI_MAKEFILE) \
		OPENAPI_DIR=$(OPENAPI_DIR) \
		OPENAPI_SRC_DIR=$(OPENAPI_SRC_DIR) \
		FILES_POSTFIX=$(FILES_POSTFIX) \
		FILES="$${SPEC}" \
		SPECTRAL_RULESET=$(SPECTRAL_RULESET) \
		OPENAPI_CASING=$(OPENAPI_CASING) \
		lint

ci-push-java:
	SPEC="$(OPENAPI_DIR)/$(OPENAPI_SRC_DIR)/$(MODULE)$(FILES_POSTFIX)"
	MVN_OPTS="-s $${CI_PROJECT_DIR}/.mvn/settings.xml"; \
	if [[ ! -v CI ]]; then
		MVN_OPTS="-Duser.home=$${HOME}"; \
	fi
	make -f $(OPENAPI_MAKEFILE) \
		OPENAPI_DIR=$(OPENAPI_DIR) \
		OPENAPI_SRC_DIR=$(OPENAPI_SRC_DIR) \
		FILES_POSTFIX=$(FILES_POSTFIX) \
		FILES="$${SPEC}" \
		MVN_OPTS="$${MVN_OPTS}" \
		push-$(VARIANT)

ci-gen-ts:
	SPEC="$(OPENAPI_DIR)/$(OPENAPI_SRC_DIR)/$${MODULE}$(FILES_POSTFIX)"
	make -f $(OPENAPI_MAKEFILE) \
		OPENAPI_DIR=$(OPENAPI_DIR) \
		OPENAPI_SRC_DIR=$(OPENAPI_SRC_DIR) \
		OPENAPI_PARAMS='$(OPENAPI_PARAMS)' \
		FILES_POSTFIX=$(FILES_POSTFIX) \
		FILES="$${SPEC}" \
		typescript-api

ci-push-ts:
	echo "@$(GROUP):registry=https://registry.npmjs.org/" >| /root/.npmrc
	echo "//registry.npmjs.org/:_authToken=$(NPM_TOKEN)" >> /root/.npmrc
	SPEC="$(OPENAPI_DIR)/$(OPENAPI_SRC_DIR)/$${MODULE}$(FILES_POSTFIX)"
	make -f $(OPENAPI_MAKEFILE) \
		OPENAPI_DIR=$(OPENAPI_DIR) \
		OPENAPI_SRC_DIR=$(OPENAPI_SRC_DIR) \
		FILES_POSTFIX=$(FILES_POSTFIX) \
		FILES="$${SPEC}" \
		NPM_TOKEN=$(NPM_TOKEN) \
		typescript-api push-typescript

config/%.yml:
	@set -e
	@if [[ -z "$(MODULE)" ]]; then
		@printf "ERROR: Missing parameter <MODULE>\n"
		exit 1
	fi
	filename="$$(basename $@)"
	@SRC="config/file-$$(echo $$filename | rev | cut -d"-" -f1 | rev)"
	cp $$SRC $@

test:
	$(MAKE) versions
	$(MAKE) libs-npm
	$(MAKE) libs-mvn-spring
	$(MAKE) libs-mvn-feign
	$(MAKE) rules-make MODULE=test
	$(MAKE) rules-ci MODULE=test
	$(MAKE) diagnose
	$(MAKE) lint-all
	$(MAKE) java-all

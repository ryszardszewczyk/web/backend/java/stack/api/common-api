const ibmCloudValidationRules = require("@ibm-cloud/openapi-ruleset");

const {
  pathSegmentCasingConvention,
  propertyCasingConvention,
  consecutivePathSegments,
  arrayResponses,
  parameterCasingConvention,
  enumCasingConvention,
  operationIdCasingConvention,
} = require("@ibm-cloud/openapi-ruleset/src/functions");
const oasDocumentSchema = require("@stoplight/spectral-rulesets").oas.rules[
  "oas3-schema"
]["then"]["function"];
const oasUnusedComponent = require("@stoplight/spectral-rulesets").oas.rules[
  "oas3-unused-component"
]["then"]["function"];
const { alphabetical, truthy } = require("@stoplight/spectral-functions");
const { oas3 } = require("@stoplight/spectral-formats");
const {
  operations,
  parameters,
  paths,
  schemas,
  unresolvedSchemas,
} = require("@ibm-cloud/openapi-ruleset-utilities/src/collections");
const IGNORE_PROP = "x-ignore";
const REUSE_SCHEMA_IGNORE_PROP = "x-rszewczyk-reuse-schema-ignore";
const REUSE_REQ_BODY_IGNORE_PROP = "x-rszewczyk-duplicated-requestBody-ignore";
const CASING_IGNORE = "x-rszewczyk-casing-ignore";
const allSchemas = [...unresolvedSchemas, "$..allOf[*]"];
const process = require("process");

const casingStyle = process.env["OPENAPI_CASING"] || "camel";

// helper functions because js is a lousy language
function log(obj) {
  const util = require("util");
  console.error(util.inspect(obj, false, null, false));
}

function logContext(context) {
  trimDocumentOutput(context["document"]);
  for (const referencedDocument of Object.values(
    context["documentInventory"]["referencedDocuments"],
  )) {
    trimDocumentOutput(referencedDocument);
  }
  context["documentInventory"]["resolver"] = "<trimmed>";
  context["documentInventory"]["resolved"] = "<trimmed>";
  for (const node of Object.values(
    context["documentInventory"]["graph"]["nodes"],
  )) {
    trimGraphNode(node);
  }
  context["documentInventory"]["graph"]["outgoingEdges"] = "<trimmed>";
  context["documentInventory"]["graph"]["incomingEdges"] = "<trimmed>";
  context["rule"] = "<trimmed>";
  log(context);
}

function trimGraphNode(node) {
  node["refMap"] = "<trimmed>";
  node["data"] = "<trimmed>";
}

function trimDocumentOutput(doc) {
  doc["input"] = "<trimmed>";
  doc["parserResult"]["ast"]["mappings"] = "<trimmed>";
  doc["parserResult"]["lineMap"] = "<trimmed>";
  doc["parserResult"]["data"] = "<trimmed>";
}

function isSubset(set, possibleSubset) {
  for (const value of possibleSubset) {
    if (!set.has(value)) {
      return false;
    }
  }
  return true;
}

function difference(iterable, setB) {
  const diff = [];
  for (const value of iterable) {
    if (!setB.has(value)) {
      diff.push(value);
    }
  }
  return diff;
}

function duplicates(iterable) {
  const seen = new Set();
  const dups = new Set();
  for (const value of iterable) {
    if (seen.has(value)) {
      dups.add(value);
    } else {
      seen.add(value);
    }
  }
  return dups;
}

function areSetsEq(set1, set2) {
  if (set1.size !== set2.size) {
    return false;
  }
  for (const item of set1) {
    if (!set2.has(item)) {
      return false;
    }
  }
  return true;
}

function last(arr) {
  if (arr.length) {
    return arr[arr.length - 1];
  }
  return null;
}

function areArrayEq(array1, array2) {
  return (
    array1.length === array2.length &&
    array1.every((value, index) => value === array2[index])
  );
}

/// openapi types helper
function refOrType(obj, modelName) {
  let value = obj["$ref"] || obj["type"];
  if (!value) {
    const allOf = obj["allOf"] || [];
    if (allOf.length == 1) {
      value = allOf[0]["$ref"];
    }
  }
  if (!value) {
    console.log(`Expected $ref, type or allOf[0].$ref in ${modelName}`);
    log(obj);
    throw `Expected $ref or type in ${modelName} ${obj}`;
  }
  return value;
}

function getModelProperties(model) {
  if (model.allOf != undefined) {
    const properties = [];
    for (const schema of model.allOf) {
      if (schema.properties == undefined) {
        continue;
      }
      for (const [key, value] of Object.entries(schema.properties)) {
        properties[key] = value;
      }
    }
    return properties;
  }
  return model.properties || [];
}

/// helper method to get requestBody definition in operations
function requestBodyDefinitionsInPaths(paths) {
  const requestBodyDefinitions = [];
  for (const [path, operations] of Object.entries(paths)) {
    for (const [method, operation] of Object.entries(operations)) {
      const requestBody = operation["requestBody"];
      if (requestBody === undefined) {
        continue;
      }
      const content = requestBody["content"];
      if (content === undefined) {
        continue;
      }
      if (content["multipart/form-data"] !== undefined) {
        // ignorowanie duplikatów multipart/form-data ze względu na błąd
        // generatora, który nie generuje modeli, gdy używa się #/components/requestBodies
        // https://github.com/OpenAPITools/openapi-generator/issues/16790
        continue;
      }
      if (requestBody[REUSE_REQ_BODY_IGNORE_PROP] !== undefined) {
        continue;
      }
      requestBodyDefinitions.push({
        definition: requestBody,
        path: ["paths", path, method, "requestBody"],
      });
    }
  }
  return requestBodyDefinitions;
}

// custom validation functions
/// 'required' property must be defined on schemas
function schemaPropertyRequiredMissing(obj) {
  if (obj.type !== "object" || "required" in obj || "allOf" in obj) {
    return [];
  }
  return [
    {
      message: '"required" property must be defined',
    },
  ];
}
/// 'required' property must be defined on schemas
function schemaPropertyTypeMissing(obj) {
  if ("allOf" in obj || "type" in obj) {
    return [];
  }
  return [
    {
      message: '"{{property}}.type" property must be defined',
    },
  ];
}

/// checks if value is capitalized
function startsCapitalized(...args) {
  const [input, _options, _context] = args;
  if (typeof input !== "string") {
    // ignore properties called like field
    return [];
  }
  const firstLetter = input.charAt(0);
  if (firstLetter.toUpperCase() !== firstLetter) {
    return [{ message: `{{property}} must start with a capital letter` }];
  }
  return [];
}

/// report descriptions without proper punctuation
const endOfSentecePunctuations = new Set([".", "?", "!", "…"]);
function validDescription(...args) {
  const [model, _options, _context] = args;
  if (typeof model !== "string" || model == "FIXME" || model == "OK") {
    // ignore properties called like field
    return [];
  }
  const desc = model.trimEnd();
  const lastChar = desc[desc.length - 1];
  if (!endOfSentecePunctuations.has(lastChar)) {
    return [
      {
        message: `{{property}} must end with punctuation (any of ${Array.from(
          endOfSentecePunctuations,
        ).join(", ")})`,
      },
    ];
  }
  return [];
}

const requiredSchemaPropertyOrder = [
  "type",
  "uniqueItems",
  "format",
  "minimum",
  "exclusiveMinimum",
  "minItems",
  "minLength",
  "maximum",
  "exclusiveMaximum",
  "maxItems",
  "maxLength",
  "default",
  "description",
  "required",
  "x-extra-annotation",
  "x-field-extra-annotation",
  "x-parent",
  "x-setter-extra-annotation",
  REUSE_REQ_BODY_IGNORE_PROP,
  REUSE_SCHEMA_IGNORE_PROP,
  IGNORE_PROP,
  CASING_IGNORE,
  "items",
  "properties",
  "additionalProperties",
  "enum",
  "allOf",
  "example",
  "$ref",
];

const requiredOperationPropertyOrder = [
  "summary",
  "description",
  "tags",
  "operationId",
  "x-codegen-request-body-name",
  IGNORE_PROP,
  "security",
  "parameters",
  "requestBody",
  "responses",
];

const requiredParameterPropertyOrder = [
  "name",
  "in",
  "required",
  "description",
  "style",
  "explode",
  "schema",
  "example",
];
/// checks if property order is correct
function propertyOrder(requiredOrder) {
  return (input) => {
    const properties = Object.keys(input);
    const propertySet = new Set(properties);
    const desiredOrder = requiredOrder.filter((property) =>
      propertySet.has(property),
    );
    if (properties.length !== desiredOrder.length) {
      const requiredOrderSet = new Set(requiredOrder);
      const unknownProperties = properties.filter(
        (prop) => !requiredOrderSet.has(prop),
      );
      return [
        {
          message: `Found unhandled properties in {{property}}: ${unknownProperties.join(
            ", ",
          )}. Rule needs to be fixed or specification is invalid.`,
        },
      ];
    }
    if (!areArrayEq(properties, desiredOrder)) {
      return [
        {
          message: `properties in model must have following order: ${desiredOrder.join(
            ", ",
          )}`,
        },
      ];
    }
    return [];
  };
}

/// checks if property order in 'required' follows declaration order in properties
function propertyOrderInRequired(...args) {
  const [input, ,] = args;
  if ("$ref" in input || input["properties"] === undefined) {
    return [];
  }
  const required = input["required"] || [];
  const requiredSet = new Set(required);
  const propertyNames = Object.keys(input["properties"]).filter((property) =>
    requiredSet.has(property),
  );

  if (!areArrayEq(required, propertyNames)) {
    const separator = "\n              - ";
    const requiredYamlList = separator + propertyNames.join(separator);
    return [
      {
        message: `declaration order in "properties" must be preserved in "required"${requiredYamlList}`,
      },
    ];
  }
  return [];
}

/// propertyCasingConvention wrapper
function propertyCasingConventionWrapper(schema, options, context) {
  if (schema[CASING_IGNORE] === true) {
    return [];
  }
  return propertyCasingConvention(schema, options, context);
}

/// checks if name is in plural form
const pluralExceptions = new Set(["children", "content", "sort"]);
function pluralName(...args) {
  const [input, , context] = args;
  if (input.type !== "array") {
    return [];
  }
  const ruleName = context.rule.name;
  const ignoredErrors = input[IGNORE_PROP] || [];
  if (ignoredErrors.includes(ruleName)) {
    return [];
  }
  const path = context.path;
  const name = path[path.length - 1];
  if (pluralExceptions.has(name)) {
    return [];
  }
  const schemaNameLowercased = name.toLowerCase();
  if (
    (!name.endsWith("s") &&
      !name.endsWith("Set") &&
      !name.endsWith("List") &&
      !name.endsWith("Array")) ||
    schemaNameLowercased.endsWith("status")
  ) {
    return [
      { message: `"${name}" is an array and must be named in plural form` },
    ];
  }

  return [];
}

/// checks if parameter is named properly
function parameterNaming(...args) {
  const [input, , context] = args;
  const parameterType = input["in"];
  let parameterTypeCapitalized =
    parameterType[0].toUpperCase() + parameterType.slice(1);
  if (parameterType === "query") {
    parameterTypeCapitalized = "";
  }
  const required = input.required || parameterType === "path";
  const path = context.path;
  const name = path[path.length - 1];
  const nameFirstLetter = name[0];
  if (nameFirstLetter === nameFirstLetter.toLowerCase()) {
    return [
      {
        message: `${name} must be capitalized.`,
      },
    ];
  }
  if (required) {
    if (!name.endsWith(parameterTypeCapitalized)) {
      return [
        {
          message: `"${name}" is a required ${parameterType} param, and thus must have '${parameterTypeCapitalized}' suffix`,
        },
      ];
    }
    if (name.endsWith("Optional")) {
      return [
        {
          message: `${name} is required, and thus musn't have 'Optional' suffix`,
        },
      ];
    }
  } else if (
    !name.endsWith("Filter") &&
    !name.endsWith(parameterTypeCapitalized + "Optional")
  ) {
    return [
      {
        message: `${name} is an optional ${parameterType} param, and thus must have '${parameterTypeCapitalized}Optional' suffix`,
      },
    ];
  }
  return [];
}

function computeSchemaKeys(prefix, spec) {
  for (const [modelName, model] of Object.entries(spec.components.schemas)) {
    if (model.type !== "object") {
      continue;
    }
    const properties = getModelProperties(model);
    const modelProperties = new Set();
    for (const [property, definition] of Object.entries(properties)) {
      let type = null;
      if (definition.type === "array") {
        type = "array." + refOrType(definition.items, modelName);
      } else {
        type = refOrType(definition, modelName);
      }
      modelProperties.add(`${property}.${type}`);
    }
    if (modelProperties.size) {
      schemaKeys[prefix + modelName] = modelProperties;
    }
  }
}

function reusablePropertiesConfig(spec) {
  const ruleConfig = spec["x-rszewczyk-property-reuse"] || {};
  return {
    minDuplicates: ruleConfig["minDuplicates"] || 2,
    ignore: new Set(ruleConfig["ignore"] || []),
  };
}

const ignoredPropertyNames = new Set([
  "description",
  "name",
  "type",
  "message",
  "value",
]);
function reusableProperties(...args) {
  const [_input, _options, context] = args;
  const propertyKeys = {};
  const spec = context.documentInventory.document.parserResult.data;
  const path = require("path");
  const specFile = context.documentInventory.document.source;
  const specFileDir = path.dirname(specFile);
  // compute candidates for reuse in current file
  computePropertyKeys("", spec, propertyKeys);
  // compute candidates for reuse in referenced files
  for (const [referencedDocument, doc] of Object.entries(
    context.documentInventory.referencedDocuments,
  )) {
    const spec = doc.parserResult.data;
    const prefix = path.relative(specFileDir, referencedDocument);
    computePropertyKeys(prefix, spec, propertyKeys);
  }
  const errors = [];
  const config = reusablePropertiesConfig(spec);
  for (const paths of Object.values(propertyKeys)) {
    if (paths.length >= config.minDuplicates) {
      errors.push({
        message: `property ${last(paths[0])} is used in ${
          paths.length
        } places, and can be reused. [${paths
          .map((a) => a.join("."))
          .join(", ")}]`,
        path: paths[0],
      });
    }
  }
  return errors;
}

function computePropertyKeys(prefix, spec, propertyKeys) {
  const config = reusablePropertiesConfig(spec);
  for (const [modelName, model] of Object.entries(
    spec.components.schemas || {},
  )) {
    if ((model.type || "object") !== "object") {
      continue;
    }
    const properties = getModelProperties(model);
    for (const [propertyName, definition] of Object.entries(properties)) {
      const type = definition.type;
      if (
        type === "object" ||
        ignoredPropertyNames.has(propertyName) ||
        config.ignore.has(propertyName)
      ) {
        continue;
      }
      if (definition["$ref"] !== undefined) {
        continue;
      }
      const modelMeta = {
        property: propertyName,
        type: type,
        default: definition["default"],
      };
      if (type === "array") {
        modelMeta["items"] = refOrType(definition.items, modelName);
        modelMeta["uniqueItems"] = definition.uniqueItems;
      } else if (type === "string") {
        modelMeta["format"] = definition.format;
        modelMeta["enum"] = definition.enum;
        modelMeta["x-dict"] = definition["x-dict"] || "";
      } else if (type === "integer") {
        modelMeta["format"] = definition.format;
        modelMeta["minimum"] = definition.minimum;
        modelMeta["maximum"] = definition.maximum;
      }

      const modelFingerprint = JSON.stringify(modelMeta);
      const paths = propertyKeys[modelFingerprint] || [];
      const path = [];
      if (prefix) {
        path.push(prefix);
      }
      path.push("components");
      path.push("schemas");
      path.push(modelName);
      path.push("properties");
      path.push(propertyName);
      paths.push(path);
      propertyKeys[modelFingerprint] = paths;
    }
  }
}

let schemaKeys = null;
function reusableDefinitions(...args) {
  const [input, , context] = args;
  if (input.type !== "object") {
    return [];
  }
  if (schemaKeys === null) {
    schemaKeys = {};
    const spec = context.documentInventory.document.parserResult.data;
    const path = require("path");
    const specFile = context.documentInventory.document.source;
    const specFileDir = path.dirname(specFile);
    computeSchemaKeys("", spec);
    for (const [referencedDocument, doc] of Object.entries(
      context.documentInventory.referencedDocuments,
    )) {
      const prefix = path.relative(specFileDir, referencedDocument) + "#";
      const spec = doc.parserResult.data;
      computeSchemaKeys(prefix, spec);
    }
  }
  const path = context.path;
  const curModelName = path[path.length - 1];
  const curModelProperties = schemaKeys[curModelName];
  if (curModelProperties === undefined) {
    return [];
  }
  const possibleAncestors = new Set();
  for (const [modelName, modelProperties] of Object.entries(schemaKeys)) {
    if (
      modelProperties.size > 1 &&
      isSubset(curModelProperties, modelProperties) &&
      !areSetsEq(curModelProperties, modelProperties)
    ) {
      possibleAncestors.add(modelName);
    }
  }
  possibleAncestors.delete(curModelName);
  for (const ignored of input[REUSE_SCHEMA_IGNORE_PROP] || []) {
    if (!possibleAncestors.has(ignored)) {
      return [
        {
          message: `"${ignored}" is not reusable for {{property}}. Please remove it from {{property}}.${REUSE_SCHEMA_IGNORE_PROP}`,
        },
      ];
    }
    possibleAncestors.delete(ignored);
  }
  if (possibleAncestors.size) {
    const possibleAscendantsFormatted =
      Array.from(possibleAncestors).join(", ");
    return [
      {
        message: `{{property}} might reuse model(s) ${possibleAscendantsFormatted}. Add '{{property}}.${REUSE_SCHEMA_IGNORE_PROP}: [${possibleAscendantsFormatted}]' to disable this check if these are false-positives.`,
      },
    ];
  }
  return [];
}

function requestBodyDefinitionDiscriminator(requestBodyDefinition) {
  const content = requestBodyDefinition["content"];
  if (content === undefined) {
    return null;
  }
  const contentTypeDiscriminators = [];
  for (const [contentType, contentDef] of Object.entries(content)) {
    const schema = contentDef["schema"];
    if (schema === undefined || !("$ref" in schema)) {
      continue;
    }
    const ref = schema["$ref"];
    contentTypeDiscriminators.push(`${contentType}.${ref}`);
  }
  if (contentTypeDiscriminators.length === 0) {
    return null;
  }
  contentTypeDiscriminators.sort();
  return contentTypeDiscriminators.join("|");
}

/// checks if model requestBody in operation could be reused
function reusableRequestBody(...args) {
  const [paths, ,] = args;
  const requestBodyDefinitions = requestBodyDefinitionsInPaths(paths);
  const redefinitions = {};
  for (const {
    definition: requestBodyDefinition,
    path,
  } of requestBodyDefinitions) {
    const discriminator = requestBodyDefinitionDiscriminator(
      requestBodyDefinition,
    );
    if (discriminator === null) {
      continue;
    }
    if (!(discriminator in redefinitions)) {
      redefinitions[discriminator] = [];
    }
    redefinitions[discriminator].push(path);
  }
  const errors = [];

  for (const [, operationPaths] of Object.entries(redefinitions)) {
    if (operationPaths.length > 1) {
      errors.push({
        message: `Found duplicated definition  [${operationPaths
          .map((a) => a.join("."))
          .join(
            ", ",
          )}]. You should extract definition to #/components/requestBodies or use already existing definition.`,
        path: operationPaths[0],
      });
    }
  }
  return errors;
}

/// checks if hardcoded model requestBody in operation is already defined in #/components/requestBodies
function duplicatedRequestBody(...args) {
  const [paths, , context] = args;
  const requestBodyDefinitions = requestBodyDefinitionsInPaths(paths);
  const components = context.document.parserResult.data.components || {};
  const componentRequestBodies = components.requestBodies || {};
  const componentRequestBodiesDiscriminators = {};

  for (const [name, definition] of Object.entries(componentRequestBodies)) {
    const discriminator = requestBodyDefinitionDiscriminator(definition);
    const path = ["components", "requestBodies", name];
    const duplicatedPath = componentRequestBodiesDiscriminators[discriminator];
    if (duplicatedPath !== undefined) {
      return [
        {
          message: `Detected duplicated requestBody. ${path.join(
            ".",
          )} that is already defined in ${duplicatedPath.join(".")}`,
        },
      ];
    }
    componentRequestBodiesDiscriminators[discriminator] = path;
  }

  const errors = [];
  for (const { definition, path } of requestBodyDefinitions) {
    const discriminator = requestBodyDefinitionDiscriminator(definition);
    const duplicatePath = componentRequestBodiesDiscriminators[discriminator];
    if (duplicatePath !== undefined) {
      errors.push({
        message: `Object request is already extracted. You should use "$ref: '#/${duplicatePath.join(
          "/",
        )}"'.`,
        path,
      });
    }
  }
  return errors;
}

const springPageKeys = new Set([
  "first",
  "last",
  "number",
  "numberOfElements",
  "size",
  "totalElements",
  "totalPages",
  "content",
]);
function springPageModelValidator(...args) {
  const [model, , context] = args;
  const path = context.path;
  const name = path[path.length - 1];
  if (!name.endsWith("Page") || name === "Page") {
    return [];
  }
  if (!("allOf" in model)) {
    return [
      {
        message:
          "Spring page model {{property}} should reuse properties with allOf",
      },
    ];
  }
  const properties = [];
  for (const [i, schema] of Object.entries(model.allOf)) {
    if (schema.properties === undefined) {
      return [{ message: `{{property}}.allOf[${i}].properties is undefined` }];
    }
    for (const [key, value] of Object.entries(schema.properties)) {
      properties[key] = value;
    }
  }
  const keySet = new Set(Object.keys(properties));
  const missingProperties = difference(springPageKeys, keySet);
  if (missingProperties.length > 0) {
    return [
      {
        message: `Spring page model {{property}} is missing properties: ${missingProperties.join(
          ", ",
        )}`,
      },
    ];
  }
  if (keySet.size !== springPageKeys.size) {
    const excessProperties = difference(keySet, springPageKeys);
    return [
      {
        message: `Spring page model {{property}} shouldn't have properties: ${excessProperties.join(
          ", ",
        )}`,
      },
    ];
  }
  const content = model["allOf"][1]["properties"]["content"];
  if (!("$ref" in content)) {
    const content_type = content["type"];
    if (content_type !== "array") {
      return [
        {
          message: `Spring page model 'content' field must be of type array, but is ${content_type} instead.`,
        },
      ];
    }
    if (content["uniqueItems"] === true) {
      return [
        {
          message: `Spring page model 'content' field must be of type array, but is a set instead.`,
        },
      ];
    }
  }
  return [];
}

const pageableDesiredOrder = ["page", "size", "sort"];
const pageableFields = new Set(pageableDesiredOrder);
/// checks if pageable definition is valid
function springPageableValidator(...args) {
  const [model, , context] = args;
  const fields = model.filter((m) => pageableFields.has(m.name));
  if (fields.length === 0) {
    return [];
  }
  const notInQuery = fields
    .filter((m) => m["in"] !== "query")
    .map((m) => m.name);
  if (notInQuery.length !== 0) {
    return [
      {
        message: `Pageable parameter(s) [${notInQuery.join(
          ", ",
        )}] must be passed in query.`,
      },
    ];
  }
  const notOptional = fields.filter((m) => m.required).map((m) => m.name);
  if (notOptional.length !== 0) {
    return [
      {
        message: `Pageable parameter(s) [${notOptional.join(
          ", ",
        )}] must be optional.`,
      },
    ];
  }
  const fieldNames = fields.map((m) => m.name);
  const missing = difference(pageableFields, new Set(fieldNames));
  if (missing.length !== 0) {
    return [
      {
        message: `Pageable parameter(s) [${missing.join(", ")}] are missing.`,
      },
    ];
  }
  if (!areArrayEq(pageableDesiredOrder, fieldNames)) {
    return [
      {
        message: `Pageable parameters must be ordered: ${pageableDesiredOrder.join(
          ", ",
        )}.`,
      },
    ];
  }
  const path = context.path;
  const doc = context.documentInventory.document.data;
  try {
    const returnType =
      doc.paths[path[1]][path[2]]["responses"]["200"]["content"][
        "application/json"
      ]["schema"]["$ref"];
    if (!returnType.endsWith("Page")) {
      return [
        {
          message: `Operations with pageable must return page model, and not ${returnType}.`,
        },
      ];
    }
  } catch {
    // pass
  }
  return [];
}

/// check if enum definition is hardcoded in fields or arrays
function hardcodedEnum(...args) {
  const [model, ,] = args;
  if (model.type === "array") {
    if ((model.items || {}).enum) {
      return [
        {
          message: "{{property}}.items.enum must be extracted",
        },
      ];
    }
  } else if (model.type === "object") {
    const properties = model.properties || {};
    const failingProperties = [];
    for (const [property, definition] of Object.entries(properties)) {
      if (definition.enum) {
        failingProperties.push(property);
      }
    }
    if (failingProperties.length) {
      return [
        {
          message: `{{property}} enum(s) must be extracted from ${failingProperties.join(
            ", ",
          )}.`,
        },
      ];
    }
  }
  return [];
}

/// check if schema definition is hardcoded
function hardcodedSchema(...args) {
  const [model, ,] = args;
  if (model.type === undefined || model.type === "boolean") {
    return [];
  }
  return [
    {
      message:
        "Schema must be extracted to #/components/schemas or shared file.",
    },
  ];
}

/// check if schema definition is hardcoded
function hardcodedParameter(...args) {
  const [model, ,] = args;
  if ("$ref" in model) {
    return [];
  }
  return [
    {
      message:
        "Parameter must be extracted to #/components/parameters or shared file.",
    },
  ];
}

function allOfDuplicatedProperties(...args) {
  const [model, ,] = args;
  if (!model.allOf) {
    return [];
  }
  const propertyNames = [];
  for (const definition of model.allOf) {
    propertyNames.push(...Object.keys(definition.properties || {}));
  }
  const duplicatedFields = duplicates(propertyNames, new Set(propertyNames));
  if (duplicatedFields.size) {
    const duplicates = Array.from(duplicatedFields).join(", ");
    return [
      {
        message: `{{property}} has duplicated fields from allOf: ${duplicates}`,
      },
    ];
  }
  return [];
}

/// report schemas with allOf that don't override description
function allOfDescriptionNotOverwritten(...args) {
  const [model, ,] = args;
  if (!model.allOf || model.description) {
    return [];
  }
  return [
    {
      message: "{{property}} misses description",
    },
  ];
}

/// report schemas with allOf that specify unneeded properties
const allOfUnneeded = ["properties", "required", "type"];
function allOfUnneededProperties(...args) {
  const [model, ,] = args;
  if (!model.allOf) {
    return [];
  }
  return allOfUnneeded
    .filter((prop) => prop in model)
    .map((prop) => {
      return { message: `{{property}}.${prop} is unnecessary in allOf model` };
    });
}

/// do not warn on empty require property
function oasDocumentSchemaWrapper(ignored) {
  return (targetVal, opts, context) => {
    let errors = oasDocumentSchema(targetVal, opts, context);

    if (!Array.isArray(errors)) {
      return [];
    }

    errors = errors.filter((err) => !ignored.has(err.message));
    return errors.filter(
      (err) => !err.message.endsWith('must have required property "$ref"'),
    );
  };
}

/// do not warn for models explicitly marked as ignored
function oasUnusedComponentWrapper(spec, opts, context) {
  const errors = oasUnusedComponent(spec, opts, context);

  const ignored = new Set();
  const ruleName = context.rule.name;
  for (const [schemaName, schema] of Object.entries(
    spec.components.schemas || {},
  )) {
    const ignoredErrors = schema[IGNORE_PROP] || [];
    if (ignoredErrors.includes(ruleName)) {
      ignored.add(`components.schemas.${schemaName}`);
    }
  }
  const unused = new Set(errors.map((err) => err.path.join(".")));
  const usedButIgnored = difference(ignored, unused);
  const filteredErrors = usedButIgnored.map((err) => {
    return {
      message: `Used component is added to ${IGNORE_PROP}.`,
      path: err.split("."),
    };
  });
  filteredErrors.push(
    ...errors.filter((err) => !ignored.has(err.path.join("."))),
  );

  return filteredErrors;
}

/// check if definition uses declarations from unshared files
function unsharedReferences(...args) {
  const [ref, ,] = args;
  if (
    ref.startsWith("#") ||
    ref.startsWith("shared/") ||
    ref.startsWith("common.yml#") ||
    ref.startsWith("stack.yml#")
  ) {
    return [];
  }
  return [
    {
      message: `Type ${ref} must be extracted to common definitions.`,
    },
  ];
}

function filterErrors(spec, context, errors) {
  const ignore = spec[IGNORE_PROP] || [];
  const ruleName = context.rule.name;
  if (ignore.includes(ruleName)) {
    if (errors.length === 0) {
      return [
        {
          message: `Suppressing ${ruleName} is not necessary. It should be deleted from ${IGNORE_PROP}.`,
          path: context.path,
        },
      ];
    }
    return [];
  }

  return errors;
}

function ibmConsecutivePathParameterSegmentsWrapper(spec, opts, context) {
  const errors = consecutivePathSegments(spec, opts, context);
  return filterErrors(spec, context, errors);
}

function ibmArrayResponsesWrapper(operation, opts, context) {
  const errors = arrayResponses(operation, opts, context);
  return filterErrors(operation, context, errors);
}

module.exports = {
  extends: ibmCloudValidationRules,
  rules: {
    "ibm-enum-casing-convention": {
      message: "{{error}}",
      resolved: true,
      given: allSchemas,
      severity: "error",
      then: {
        function: enumCasingConvention,
        functionOptions: { type: "macro" },
      },
    },
    "ibm-operationid-casing-convention": {
      message: "{{error}}",
      resolved: true,
      given: operations,
      severity: "error",
      then: {
        function: operationIdCasingConvention,
        functionOptions: { type: casingStyle },
      },
    },
    "ibm-parameter-casing-convention": {
      message: "{{error}}",
      resolved: true,
      given: parameters,
      severity: "error",
      then: {
        function: parameterCasingConvention,
        functionOptions: { type: casingStyle },
      },
    },
    "ibm-property-casing-convention": {
      given: schemas,
      then: {
        function: propertyCasingConventionWrapper,
        functionOptions: { type: casingStyle },
      },
      severity: "error",
    },
    "ibm-path-segment-casing-convention": {
      message: "{{error}}",
      resolved: true,
      given: paths,
      severity: "error",
      then: {
        function: pathSegmentCasingConvention,
        functionOptions: {
          type: "kebab",
        },
      },
    },
    "ibm-no-consecutive-path-parameter-segments": {
      description:
        "Path strings should not contain two or more consecutive path parameter references",
      message: "{{error}}",
      given: paths,
      severity: "error",
      resolved: true,
      then: {
        function: ibmConsecutivePathParameterSegmentsWrapper,
      },
    },
    "ibm-no-array-responses": {
      description:
        "Operations should not return an array as the top-level structure of a response.",
      message: "{{error}}",
      given: operations,
      severity: "error",
      resolved: true,
      then: {
        function: ibmArrayResponsesWrapper,
      },
    },
    "rszewczyk-oas3-schema": {
      description: "Validate structure of OpenAPI v3 specification.",
      message: "{{error}}.",
      severity: "error",
      formats: [oas3],
      given: "$",
      then: {
        function: oasDocumentSchemaWrapper(
          new Set(['"required" property must not have fewer than 1 items']),
        ),
      },
    },
    "rszewczyk-oas3-unused-component": {
      description:
        "Overrides default rule for detecting unused components, to ignore errors for object left by design.",
      message: "{{error}}",
      resolved: false,
      severity: "error",
      given: "$",
      then: [
        {
          function: oasUnusedComponentWrapper,
        },
      ],
    },
    // disabled
    // requires (min|max)Items
    "ibm-array-attributes": "off",
    // property name must be plural name of $ref object
    "ibm-collection-array-property": "off",
    // forbids content type `*/*`
    "ibm-content-type-is-specific": "off",
    // requires conformance with IBM API handbook compliance
    "ibm-error-response-schemas": "off",
    // requires method version in path
    "ibm-major-version-in-path": "off",
    // forbids array of arrays structures
    "ibm-no-array-of-arrays": "off",
    // forbids recursive structures
    "ibm-no-circular-refs": "off",
    // requires ids in form verb_noun
    "ibm-operationid-naming-convention": "off",
    // requires IBM-style pagination
    "ibm-pagination-style": "off",
    "ibm-patch-request-content-type": "off",
    // get parameters should return same response as other operations
    "ibm-resource-response-consistency": "off",
    // each non-204 resp should have body (false positive for NoContent response)
    "ibm-response-status-codes": "off",
    "ibm-request-and-response-content": "off",
    "ibm-schema-naming-convention": "off",
    // requires pattern, minLength and maxLength
    "ibm-string-attributes": "off",
    // requires example (worth enabling)
    "ibm-success-response-example": "off",
    "ibm-unique-parameter-request-property-names": "off",
    "ibm-well-defined-dictionaries": "off",
    "ibm-no-ambiguous-paths": "off",
    // requires specifying servers
    "oas3-api-servers": "off",
    // overwritten to silence some warnings
    "oas3-schema": "off",
    // overwritten to silence some warnings
    "oas3-unused-component": "off",

    "ibm-avoid-inline-schemas": "error",
    "ibm-avoid-repeating-path-parameters": "error",
    "ibm-binary-schemas": "error",
    "ibm-error-content-type-is-json": "error",
    "ibm-no-duplicate-description-with-ref-sibling": "error",
    "ibm-no-operation-requestbody": "error",
    "ibm-no-optional-properties-in-required-body": "error",
    "ibm-openapi-tags-used": "error",
    "ibm-operation-summary": "error",
    "ibm-parameter-description": "error",
    "ibm-parameter-order": "error",
    "ibm-property-description": "error",
    "ibm-ref-pattern": "error",
    "ibm-requestbody-name": "error",
    "ibm-schema-description": "error",
    "ibm-schema-type": "error",
    "ibm-securityschemes": "error",
    "ibm-summary-sentence-style": "error",
    "info-description": "error",
    "no-$ref-siblings": "error",
    "oas3-parameter-description": "error",
    "openapi-tags": "error",
    "operation-description": "error",
    "operation-tag-defined": "error",
    "operation-tags": "error",

    // custom rules
    "rszewczyk-parameters-alphabetical": {
      description: "Checks if parameters are defined in alphabetical order.",
      message: "{{error}}",
      resolved: true,
      severity: "error",
      given: "$.components.parameters",
      then: [
        {
          function: alphabetical,
        },
      ],
    },
    "rszewczyk-schemas-alphabetical": {
      description: "Checks if schemas are defined in alphabetical order.",
      message: "{{error}}",
      resolved: true,
      severity: "error",
      given: "$.components.schemas",
      then: [
        {
          function: alphabetical,
        },
      ],
    },
    "rszewczyk-missing-schema-required": {
      description:
        'Ensured that field "required" is always defined in schemas.',
      message: "{{error}}",
      resolved: true,
      severity: "error",
      given: allSchemas,
      then: [
        {
          function: schemaPropertyRequiredMissing,
        },
      ],
    },
    "rszewczyk-missing-schema-type": {
      description: 'Ensures that field "type" is always defined in schemas.',
      message: "{{error}}",
      resolved: false,
      severity: "error",
      // doesn't work with JSONPath in 'schemas' variable
      given: "$.components.schemas[*]",
      then: [
        {
          function: schemaPropertyTypeMissing,
        },
      ],
    },
    "rszewczyk-valid-description": {
      description: "Checks for description misspelling (for now punctuation).",
      message: "{{error}}",
      severity: "error",
      given: "$..description",
      then: [
        {
          function: validDescription,
        },
      ],
    },
    "rszewczyk-capitalized": {
      description: "Checks if description starts with capitalized letter.",
      message: "{{error}}",
      severity: "error",
      given: ["$..description", "$..summary"],
      then: [
        {
          function: startsCapitalized,
        },
      ],
    },
    "rszewczyk-collection-name-plural": {
      description: "Checks if array fields use plural forms in their names.",
      message: "{{error}}",
      severity: "error",
      given: [
        "$.components.schemas[*]",
        "$.components.schemas[*].properties[*]",
      ],
      then: [
        {
          function: pluralName,
        },
      ],
    },
    "rszewczyk-use-declaration-order-in-required": {
      description:
        'Enforces order in "required" in line with field declarations.',
      message: "{{error}}",
      resolved: false,
      severity: "error",
      given: allSchemas,
      then: [
        {
          function: propertyOrderInRequired,
        },
      ],
    },
    "rszewczyk-schema-wrong-property-order": {
      description: "Enforces order of fields in schema definition.",
      message: "{{error}}",
      resolved: true,
      severity: "error",
      given: [...allSchemas, "$..properties[*]"],
      then: [
        {
          function: propertyOrder(requiredSchemaPropertyOrder),
        },
      ],
    },
    "rszewczyk-operation-wrong-property-order": {
      description: "Enforces order of fields in operation definition.",
      message: "{{error}}",
      resolved: true,
      severity: "error",
      given: operations,
      then: [
        {
          function: propertyOrder(requiredOperationPropertyOrder),
        },
      ],
    },
    "rszewczyk-parameter-wrong-property-order": {
      description: "Enforces order of fields in property definition.",
      message: "{{error}}",
      resolved: true,
      severity: "error",
      given: "$.components.parameters[*]",
      then: [
        {
          function: propertyOrder(requiredParameterPropertyOrder),
        },
      ],
    },
    "rszewczyk-parameter-naming": {
      description: "Enforces parameter naming convention.",
      message: "{{error}}",
      resolved: true,
      severity: "error",
      given: "$.components.parameters[*]",
      then: [
        {
          function: parameterNaming,
        },
      ],
    },
    "rszewczyk-reuse-schema": {
      description: "Checks reusability of schema definitions.",
      message: "{{error}}",
      resolved: true,
      severity: "error",
      given: "$.components.schemas[*]",
      then: [
        {
          function: reusableDefinitions,
        },
      ],
    },
    "rszewczyk-reuse-requestBody": {
      description: "Checks reusability of requestBody definitions.",
      message: "{{error}}",
      resolved: false,
      severity: "error",
      given: "$.paths",
      then: [
        {
          function: reusableRequestBody,
        },
      ],
    },
    "rszewczyk-duplicated-requestBody": {
      description:
        "Checks creating new requestBody definition, that was already defined in #/components/schemas/requestBodies.",
      message: "{{error}}",
      resolved: false,
      severity: "error",
      given: "$.paths",
      then: [
        {
          function: duplicatedRequestBody,
        },
      ],
    },
    "rszewczyk-reuse-properties": {
      description:
        "Checks for reusability of schema properties. Rule can be adjusted with a help of an object np. x-rszewczyk-property-reuse: {minDuplicates: 3, ignore: [query]}.",
      message: "{{error}}",
      resolved: true,
      severity: "error",
      given: "$",
      then: [
        {
          function: reusableProperties,
        },
      ],
    },
    "rszewczyk-invalid-spring-page-model": {
      description:
        "Checks pageable models that do not align with Spring page model.",
      message: "{{error}}",
      resolved: true,
      severity: "error",
      given: "$.components.schemas[*]",
      then: [
        {
          function: springPageModelValidator,
        },
      ],
    },
    "rszewczyk-invalid-spring-pageable": {
      description:
        "Checks operation parameters that do not align with Spring Pageable model.",
      message: "{{error}}",
      resolved: true,
      severity: "error",
      given: "$.paths[*][*].parameters",
      then: [
        {
          function: springPageableValidator,
        },
      ],
    },
    "rszewczyk-allOf-duplicates": {
      description:
        "Checks for unnecessary fields definition, that are already defined with allOf.",
      message: "{{error}}",
      resolved: true,
      severity: "error",
      given: "$.components.schemas[*]",
      then: [
        {
          function: allOfDuplicatedProperties,
        },
      ],
    },
    "rszewczyk-allOf-description-not-overwritten": {
      description: "Checks non overwritten description, when allOf is used.",
      message: "{{error}}",
      resolved: true,
      severity: "error",
      given: "$.components.schemas[*]",
      then: [
        {
          function: allOfDescriptionNotOverwritten,
        },
      ],
    },
    "rszewczyk-allOf-unneeded-properties": {
      description: "Checks unnecessary or ignored fields, when allOf is used.",
      message: "{{error}}",
      resolved: true,
      severity: "error",
      given: "$.components.schemas[*]",
      then: [
        {
          function: allOfUnneededProperties,
        },
      ],
    },
    "rszewczyk-schema-not-extracted": {
      description:
        "Checks schemas, that are hardcoded, instead of being reused with $ref.",
      message: "{{error}}",
      resolved: false,
      severity: "error",
      given: schemas,
      then: [
        {
          function: hardcodedSchema,
        },
      ],
    },
    "rszewczyk-parameter-not-extracted": {
      description:
        "Checks parameters that are hardcoded in operation definition instead of in #/components/parameters.",
      message: "{{error}}",
      resolved: false,
      severity: "error",
      given: parameters,
      then: [
        {
          function: hardcodedParameter,
        },
      ],
    },
    "rszewczyk-enum-not-extracted": {
      description:
        "Checks definition enumerations, that are not extracted to separate schema.",
      message: "{{error}}",
      resolved: false,
      severity: "error",
      given: "$.components.schemas[*]",
      then: [
        {
          function: hardcodedEnum,
        },
      ],
    },
    "rszewczyk-unshared-reference": {
      description: "Checks using references from files, that are not shared.",
      message: "{{error}}",
      resolved: false,
      severity: "error",
      given: "$..$ref",
      then: [
        {
          function: unsharedReferences,
        },
      ],
    },
  },
};

const config = require("./ruleset");

const rules = config["rules"];
rules["oas3-unused-component"] = "off";
rules["openapi-tags"] = "off";
delete rules["rszewczyk-reuse-schema"];
delete rules["rszewczyk-oas3-schema"];
delete rules["rszewczyk-oas3-unused-component"];

module.exports = config;

include common.mk
FILES_POSTFIX := .yml
OPENAPI_MAKEFILE := /opt/makefile/Makefile

GROUP := stack.shared_models

OPENAPI_DIR := .
OPENAPI_SRC_DIR := .
OPENAPI_PARAMS := "--skip-validate-spec"
SPECTRAL_RULESET := $(OPENAPI_DIR)/spectral.js
ARTIFACTORY_DEPLOY_URL := https://gitlab.com/api/v4/projects/57777031/packages/maven
JAVA_VERSION := 21

lint:
	@$(MAKE) docker-lint MODULE=common
	@$(MAKE) docker-lint MODULE=stack

common-java: common-spring common-feign

common-spring:
	@$(MAKE) docker-build VARIANT=spring MODULE=common
	@$(MAKE) install-java VARIANT=spring MODULE=common

common-feign:
	@$(MAKE) docker-build VARIANT=feign MODULE=common
	@$(MAKE) install-java VARIANT=feign MODULE=common

common-lint:
	@$(MAKE) docker-lint MODULE=common

stack-java: stack-spring stack-feign

stack-spring:
	@$(MAKE) docker-build VARIANT=spring MODULE=stack IMPORT_MAPPINGS_EXCLUDE_FILES=common.yml EXTRA_DEPS='$(OPENAPI_DIR)/$(OPENAPI_SRC_DIR)/common.yml'
	@$(MAKE) install-java VARIANT=spring MODULE=stack EXTRA_DEPS='$(OPENAPI_DIR)/$(OPENAPI_SRC_DIR)/common.yml'

stack-feign:
	@$(MAKE) docker-build VARIANT=feign MODULE=stack IMPORT_MAPPINGS_EXCLUDE_FILES=common.yml EXTRA_DEPS='$(OPENAPI_DIR)/$(OPENAPI_SRC_DIR)/common.yml'
	@$(MAKE) install-java VARIANT=feign MODULE=stack EXTRA_DEPS='$(OPENAPI_DIR)/$(OPENAPI_SRC_DIR)/common.yml'

stack-lint:
	@$(MAKE) docker-lint MODULE=stack EXTRA_DEPS='$(OPENAPI_DIR)/$(OPENAPI_SRC_DIR)/common.yml'

